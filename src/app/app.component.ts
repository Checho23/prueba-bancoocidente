import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators, FormControl } from '@angular/forms';
import {Observable} from 'rxjs';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';
import { DataService } from './services/data.service';
import { City } from './models/city.model';
import { UserData } from './models/userData.model';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{


  public model: any;
  public cities:City[] = [];
  public userdata: FormGroup;
  public person: UserData;
  constructor(private dataService: DataService,private fb: FormBuilder){}

  ngOnInit()
  {
    this.getCities();
    this.userdata = this.fb.group({
      first_name: new FormControl('', [Validators.required] ),
      first_lastname: new FormControl('', [Validators.required ]),
      document: new FormControl('', [Validators.required, Validators.minLength(4) ]),
      city: new FormControl('', [Validators.required ]),
      phone: new FormControl('', [Validators.required ]),
      email: new FormControl('', [Validators.required, Validators.email ]),
   });
  }

  
  get first_name() { return this.userdata.get('first_name'); }

  get document() { return this.userdata.get('document'); }

  private getCities()
  {
    this.dataService.sendGetRequest('cities').subscribe((data:City[])=>
    {
      this.cities = data;
      
    });
  }
  sendForm()
  {
    if(this.userdata.valid)
    {
      this.person =  new UserData;
      this.person.first_name = this.userdata.get('first_name').value;
      this.person.first_lastname = this.userdata.get('first_lastname').value;
      this.person.document = this.userdata.get('document').value;
      this.person.fk_city = this.userdata.get('city').value.id;
      this.person.phone = this.userdata.get('phone').value;
      this.person.email = this.userdata.get('email').value;

      console.log(this.person);
      alert('formulario enviado');
    }
    else
      alert('Todos los campos son obligatorios');
  }

  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      map(term => term.length < 3 ? []
        : this.cities.filter(c => c.city_name.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    );
  
  formatter = (x:{city_name:string})=>x.city_name;
}
