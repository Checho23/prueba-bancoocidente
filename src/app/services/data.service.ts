import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class DataService {

  private REST_API_SERVER = "http://localhost:3000/";

  constructor(private httpClient: HttpClient) { }

  
  public sendGetRequest(route)
  {
    let url = `${this.REST_API_SERVER}`+route;
    return this.httpClient.get(url);
  }
}
