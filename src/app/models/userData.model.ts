export class UserData 
{
    first_name:string;
    first_lastname:string;
    document:number;
    fk_city:number;
    phone:number;
    email:string;
}